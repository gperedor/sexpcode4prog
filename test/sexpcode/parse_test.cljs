(ns sexpcode.parse-test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [sexpcode.parse :as p]
            [sexpcode.types :as t]))

(deftest analize-quote-test
  (testing "single quote form"
    (is (= (p/analize-quote "{quote implying}")
           ["" (t/->Quote (t/->Node "implying" ""))])))
  (testing "quote containing markup"
    (is (= (p/analize-quote "{quote {i implying}}")
           ["" (t/->Quote (t/->Node (t/->Markup "i"
                                                (t/->Node "implying"
                                                          ""))
                                    ""))]))))

(deftest analize-markup
  (testing "single tag form"
    (is (= (p/analize-markup "{b strongly worded statement}")
           ["" (t/->Markup "b" (t/->Node "strongly worded statement" ""))])))
  (testing "composed tags"
    (is (= (p/analize-markup "{b.i pedantic}")
           ["" (t/->Markup "b"
                           (t/->Markup "i"
                                       (t/->Node "pedantic" "")))]))))

(deftest analize-text
  (testing "empty text"
    (is (= (p/analize-text ""))
        ["" ""]))
  (testing "normal text"
    (is (= (p/analize-text "normal text"))
        ["" "normal-text"]))
  (testing "mixed text"
    (is (= (p/analize-text "Hello, {b Ahmed}"))
        ["{b Ahmed}" "Hello, "])))

(deftest analize-code
  (testing "single delimiter"
    (is (= (p/analize-code "{code% clojure (println \"Hello, world!\") %}")
           ["" (t/->Code "clojure" "(println \"Hello, world!\")")])))
  (testing "complex delimiter"
    (is (= (p/analize-code "{code%1 clojure (+ 1 2) %1}")
           ["" (t/->Code "clojure" "(+ 1 2)")])))
  (testing "multiline code"
    (is (= (p/analize-code "{code%1 clojure
(+ 1 2)
(println \"whatever\")
%1}")
          ["" (t/->Code "clojure" "(+ 1 2)
(println \"whatever\")")])))
  (testing "delimiter appears in text"
    (is (= (p/analize-code "{code#1 clojure (+ 1 2) #1 #1}")
           ["" (t/->Code "clojure" "(+ 1 2) #1")]))

    (is (= (p/analize-code "{code#1 clojure (+ 1 2) #1} #1")
           [" #1" (t/->Code "clojure" "(+ 1 2)")]))))

(deftest test-verbatim
  (testing "single delimiter"
    (is (= (p/analize-verbatim "{# this is verbatim text }}}{ #}")
           ["" (t/->Verbatim "this is verbatim text }}}{")]))
    (is  (= (p/analize-verbatim "{# this is verbatim text }}}{
#}")
            ["" (t/->Verbatim "this is verbatim text }}}{")]))))
