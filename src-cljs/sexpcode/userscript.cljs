(ns sexpcode.userscript
  (:require [sexpcode.parse :as parse]
            [sexpcode.bbcode :as bbcode]))


(defn bbcode-read
  [expr]
  (let [[remainder ast] (parse/analize-sexp expr)]
    (if (empty? remainder)
      [(bbcode/translate ast) 'success]
      [(str  "Syntax error: " remainder) 'error])))


(defn tables
  []
  (.getElementsByClassName js/document "postform"))

(defn create-element
  [tag-name]
  (.createElement js/document tag-name))

(defn button-row
  []
  (let [row (.createElement js/document "tr")
        button (doto (create-element "button")
                 (.setAttribute "type" "button")
                 (.appendChild (.createTextNode js/document "Expand!")))
        button-cell (doto (create-element "td")
                      (.setAttribute "style" "text-align:right")
                      (.appendChild button))]
    (doto row
      (.appendChild (create-element "td"))
      (.appendChild (create-element "td"))
      (.appendChild (create-element "td"))
      (.appendChild (create-element "td"))
      (.appendChild button-cell))))

(defn insert-row!
  [table]
  (let [row (button-row)
        button (.item (.getElementsByTagName row "button") 0)
        textarea (.item (.getElementsByTagName table "textarea") 0)
        insertion-row (-> (.-parentNode textarea)
                          .-parentNode
                          .-nextElementSibling)]

    (set! (.-width (.-style textarea)) "100%")
    (.addEventListener button "click"
         (fn [e]
           (let [
                 previous-value (.-value textarea)
                 [result status] (bbcode-read previous-value)]
             (if (= status 'error)
               (set! (.-value textarea) (str previous-value "\n" result))
               (set! (.-value textarea)  result)))))
    (-> (.-lastChild table)
        (.insertBefore row insertion-row))))


(defn js-for-each
  [f getter coll]
  (let [length (.-length coll)]
    (loop [i 0]
      (when (< i length)
        (do (f (getter coll i))
            (recur (inc i)))))))

(defn ^:export take-over!
  []
  (js-for-each insert-row! (fn [c i] (.item c i)) (tables)))

