(ns sexpcode.types)

(defrecord Markup   [tag body])
(defrecord Verbatim [body])
(defrecord Quote    [body])
(defrecord Node     [left right])
(defrecord Code     [language body])
