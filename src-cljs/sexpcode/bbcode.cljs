(ns sexpcode.bbcode
  (:require [clojure.string :as string]
            [goog.string :as gs]
            goog.string.format))

(defmulti evaluate type)

(defn translate
  [ast]
  (if (empty? ast)
        ""
        (evaluate ast)))

(defmethod evaluate js/String
  [string]
  string)

(defmethod evaluate sexpcode.types/Markup
  [markup]
  (gs/format "[%s]%s[/%s]"
          (:tag markup)
          (translate  (:body markup))
          (:tag markup)))

(defmethod evaluate sexpcode.types/Node
  [node]
  (str (translate (:left node))
       (translate (:right node))))

(defmethod evaluate sexpcode.types/Code
  [code]
  (gs/format "[code]%s[/code]"
          (:body code)))

(defmethod evaluate sexpcode.types/Verbatim
  [verbatim]
  (:body verbatim))

(defn quote-body-transmogrify
  "Support for Shiichan's broken blockquotes"
  [body]
  (let [[head tail :as result] (string/split (translate body) #"\n" 2)]
    (if (> (count result) 1)
      (str head "[o]\n" tail "[/o]")
      head)))

(defmethod evaluate sexpcode.types/Quote
  [quote]
  (str "> " (quote-body-transmogrify (:body quote))))
