(ns sexpcode.core
  (:require [sexpcode.parse :as parse]
            [sexpcode.bbcode :as bbcode]))

;(bbcode-read (format "{spoiler %1$s%2$s%3$s}" (enterprise  "EXPERT ") bbcode (enterprise " PROGRAMMERS")))

;(analize-sexp "{%%% {spoiler /prog/} pgppppppppp %%%} nyah")

(def torture
"{%%% verbatim }}}} %%%} {code% clojure ((scheme)) %} asdjfladfjlaj {spoiler {b.i.u.o temas}}
{quote hello
{quote world
{i !}}} ")

(defn ^:export  bbcode-read
  [expr]
  (let [[remainder ast] (parse/analize-sexp expr)]
    (if (empty? remainder)
      (bbcode/translate ast)
      (str  "Syntax error: " remainder))))
