(ns sexpcode.parse
  (:use [sexpcode.types :only [->Code ->Markup ->Node ->Quote ->Verbatim]])
  (:require [clojure.string :as string]
            [goog.string :as gs]
            goog.string.format)
  (:use-macros [sexpcode.util :only [choose cond-let]]))


;;; Incomplete and wrong, don't bother with this grammar.
;; S -> Text, S | Markup, S | ""
;; Text, "{"| "}" -> <any character excluding unescaped "{" and "}">, Text
;; Markup -> "{", Tag, {".", Tag}, S, "}"
;; Verbatim -> "{", <non alphanumeric character>*, <any character>*,
;; <non alphanumeric character>*, "}"
;; Tag -> "s"|"m"|"b"|"i"|"u"|"o"|"sup"|"sub"|"code"|"spoiler"
;; Quote -> "{quote", S, "}"
;; Code -> "{code", <non alphanumeric>, <any character>* ...

;; String -> [Text|Markup]

(declare analize-sexp)

(defn test-result
  "Handles the [remainder, node] convention by either returning the result, or false"
  [parser expr]
  (let [[remainder new-node] (parser expr)]
    (if (not= new-node 'error)
      [remainder new-node]
      false)))

(defn test-text
  "Handles the [remainder, node] convention by either returning the result, or false. The edge case of text is that of not reading anything for a node, so it's different from test-result"
  [text-parser expr]
  (let [[remainder new-node] (text-parser expr)]
    (if (not= new-node "")
      [remainder new-node]
      false)))


(def text-delimiters
  #{nil \{ \}})


(defn process-tags
  "Makes a neat list of tags. Repeats iterated tags"
  [raw-tags]
  (let [chunks (string/split raw-tags #"\.")]
    (flatten (map (fn [chunk]
                    (if-let [[_ tag times] (re-find #"(\w+)(?:\*|\^)(\d+)" chunk)]
                      (repeat (js/parseInt times) tag)
                      chunk))
                  chunks))))

(defn extract-tags
  [expr]
  (let [[raw-tags after-tags]  (string/split expr #"\s" 2)]
    [after-tags  (process-tags (subs raw-tags 1))]))

(defn make-markup
  [body tag]
  (->Markup tag body))

;;; The format pattern string is cljs-specific, same in analize-code
(defn analize-verbatim
  "Verbatim. {delimiter stuff delimiter}"
  [expr]
  (if-not (re-find #"^\{\W.*\s(\s|.*)" expr)
    [expr 'error]
    (let [[_ tag] (re-find #"^\{(\W[^\s]*)" expr)
          [_ tail] (string/split expr, (re-pattern (gs/format "\\{%s\\s" tag)), 2)
          [contents remainder :as result] (string/split tail, (re-pattern (gs/format "\\s%s\\}" tag)), 2)]
      ;; if <= 1, there wasn't an end tag
      (if (> (count result) 1)
        [remainder (->Verbatim contents)]
        [expr 'error]))))

(defn analize-code
  "Code syntax analizer, requires a delimiter, unlike the original sexpcode spec"
  [expr]
  (let [chunk-regex #"^\{code(\W\S*)\s+(\S+)\s((.|\s)*)"
        [_ delimiter language tail]  (re-matches chunk-regex expr)]
    (if-not (every? (comp not nil?) [delimiter language tail])
      [expr 'error]
      (let [split-regex (re-pattern (gs/format "\\s%s\\}" delimiter))
            [body remainder :as result] (string/split tail split-regex 2)]
        ;; if <= 1, then ye olde "EOF found while parsing"
        (if (> (count result) 1)
          [remainder (->Code language body)]
          [expr 'error])))))

(defn analize-quote
  "Quote. It must be preceded by newlines, obviously."
  [expr]
  (if-let [[_ tail] (re-matches #"\{quote\s((\s|.)*)" expr)]
    (let [[remainder new-node] (analize-sexp tail)]
      (if (and (not= new-node 'error) (= (first remainder) \}))
        [(subs remainder 1) (->Quote new-node)]
        [expr 'error]))
    [expr 'error]))

(defn analize-markup
  "Markup syntactical form. Recursively matches braces and parses the body with analize-sexp"
  [expr]
  (if-not (re-find #"^\{\w(\s|.)*" expr)
    [expr 'error]
    (let [[after-tags tags] (extract-tags expr)
          [remainder new-node] (analize-sexp  after-tags)]
      (if (and (not= new-node 'error) (= (first remainder) \}))
        [(subs remainder 1) (reduce make-markup new-node (reverse tags))]
        [expr 'error]))))

(defn analize-text
  "Text syntactical form. Loops until it finds an unescaped brace"
  [expr]
  (loop [remaining-expr expr
         ast            ""
         state          'base]
    (let [char (first remaining-expr)]
      (condp = state
        'base  (cond
                (= char \\)
                (recur (subs remaining-expr 1)
                       ast
                       'slash)

                (contains? text-delimiters char)
                [remaining-expr ast]

                :else
                (recur (subs remaining-expr 1)
                       (str ast char)
                       'base))

        'slash (let [current (if (or (= char \{)
                                     (= char \}))
                               char
                               (str \\ char))]
                 (recur (subs remaining-expr 1)
                        (str ast current)
                        'base))))))


(defn analize-sexp
  "Root syntactical analizer, tries to exhaust all the different parsers.
Parsers follow the convention of String -> [String, Parse|'error]"
  [expr]
  (if (empty? expr)
        ["" ""]
        (let [first-production
              (choose (test-result analize-verbatim expr)
                      (test-result analize-code     expr)
                      (test-result analize-quote    expr)
                      (test-result analize-markup   expr))]

          (cond-let [[remainder complex-parses] first-production]
                    (if-let [[final-remainder final-parse] (test-result analize-sexp  remainder)]
                      [final-remainder  (->Node complex-parses final-parse)]
                      [expr 'error])

                    [[remainder text-node] (test-text analize-text expr)]
                    (if-let [[final-remainder final-parse] (test-result analize-sexp remainder)]
                      [final-remainder (->Node text-node final-parse)]
                      [remainder text-node])
                    (vector expr "")))))
