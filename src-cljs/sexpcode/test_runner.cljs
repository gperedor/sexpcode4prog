(ns sexpcode.test-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [sexpcode.parse-test]))

(doo-tests 'sexpcode.parse-test)
