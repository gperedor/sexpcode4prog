(ns sexpcode.util)

(defmacro dbg[x] `(let [x# ~x] (pprint/pprint (str "dbg:" '~x "=" x#)) x#))

(defmacro cond-let
  "Usage: (cond-let bindings1 then1
                    bindings2 then2
                    ...
                    default-value)

  Like cond, but with the same binding syntax as if-let. It may take an optional
  default, a single value that's not a vector literal"
  [& clauses]
  (when (not-empty clauses)
    (let [[predicate consequent & the-rest] clauses]
      (if (vector? predicate)
        `(if-let [~(first predicate) ~(second predicate)]
           ~consequent
           (cond-let ~@the-rest))
        `(if ~predicate
           ~predicate
           ~(if (and (nil? consequent) (empty? the-rest))
              nil
              `(cond-let ~@(cons consequent the-rest))))))))

(defmacro choose
  "Just a fall-through chain of expressions evaluating to truthy values; evaluates to the first true value"
  [& clauses]
  (when (not-empty clauses)
    `(if-let [x# ~(first clauses)]
       x#
       (choose ~@(rest clauses)))))
