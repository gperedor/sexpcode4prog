# sexpcode4prog

A [Sexpcode](http://cairnarvon.rotahall.org/misc/sexpcode.html)
implementation for use in the comment boxes of 4chan's defunct
``prog``. The userscript contained added a button which allowed to
translate sane sexpcode into Shiichan's utterly broken BBCode markup.

It deviates from the spec in the following ways:

 - The code tag is affixed with an arbitrary delimiter; it must be
   used like  this:

```
{code# clojure (+ 1 2) #}
```

- Likewise, the code tag requires the ending brace to be preceded
  by some type of blank character.

- No function definition support. They are silly.

- No whitespace in "arguments".


## Usage

The board no longer exists, so the userscript is useless. Bummer.

## Tests

Having [Node.js](https://nodejs.org/en/) version 0.12 or higher
installed, [lein](http://leiningen.org/) and a suitable JDK, issue:

```bash
lein doo node test
```

## License

Copyright © 2012 FIXME

Distributed under the Eclipse Public License, the same as Clojure.
